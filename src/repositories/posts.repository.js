import axios from "axios";

export class PostRepository{
    async getAllPosts(){
        return await( await axios.get('https://jsonplaceholder.typicode.com/posts')).data;
    }
    async sendPost(postData){
        return await( await axios.post('https://jsonplaceholder.typicode.com/posts'), postData);
    }
    async delPost(postId){
        return await axios.delete(`https://jsonplaceholder.typicode.com/posts/${postId}`);
    }
}