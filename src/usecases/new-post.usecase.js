import { Post } from "../model/post";
import { PostRepository } from "../repositories/posts.repository";

export class NewPostUseCase {
    static async execute (postData){
        const repository = new PostRepository(); 
        const response = await repository.sendPost(postData);
        console.log(response);
        return new Post({
            id:response.id,
            title: response.title,
            content : response.body,
        });
    }
}