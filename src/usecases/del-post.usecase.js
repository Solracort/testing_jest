import { Post } from "../model/post";
import { PostRepository } from "../repositories/posts.repository";

export class DelPostUseCase {
    static async execute (postId){
        const repository = new PostRepository(); 
        const response = await repository.delPost(postId);
        
        return response;
    }
}