import { Post } from "../model/post";
import { PostRepository } from "../repositories/posts.repository";

export class OddPostsUseCase {
    static async execute (){
        const repository = new PostRepository(); 
        const posts = await repository.getAllPosts();
        return posts
            .filter(post => post.id%2 !==0)
            .map(post => new Post({
            id: post.id,
            title: post.title,
            content : post.body,
        }));
    }
}