import { DelPostUseCase } from "../src/usecases/del-post.usecase";

describe ('Del a post use case', ()=>{
    it ('should del a post with postId', async ()=>{
        const postId = 5;
        const response = await DelPostUseCase.execute(postId);   
        expect(response.status).toBe(200);
        expect(response.config.method).toBe('delete');
        expect(response.config.url).toBe(`https://jsonplaceholder.typicode.com/posts/${postId}`);
    }, 20000);

});