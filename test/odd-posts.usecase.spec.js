import { OddPostsUseCase  } from "../src/usecases/odd-posts.usecase";
import { PostRepository } from "../src/repositories/posts.repository.js";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");

describe ('Odd posts use case', ()=>{

    beforeEach(()=>{
        PostRepository.mockClear();
    });

    it ('should get odd posts', async ()=>{
        PostRepository.mockImplementation(()=>{
            return {
                getAllPosts:()=>{
                    return POSTS.filter(post => post.id%2 !==0);
                },
            };
        });
        const posts= await OddPostsUseCase.execute() ;
        
        expect (posts.length).toBe(50);

        expect(posts[0].id).toBe(POSTS[0].id);
        expect(posts[0].content).toBe(POSTS[0].body);
    });
});