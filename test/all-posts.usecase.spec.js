import { AllPostsUseCase } from "../src/usecases/all-posts.usecase";
import { PostRepository } from "../src/repositories/posts.repository.js";
import { POSTS } from "./fixtures/posts";

jest.mock("../src/repositories/posts.repository");

describe ('All posts use case', ()=>{

    beforeEach(()=>{
        PostRepository.mockClear();
    });

    it ('should get posts with odd id', async ()=>{
        PostRepository.mockImplementation(()=>{
            return {
                getAllPosts:()=>{
                    return POSTS;
                },
            };
        });
        const posts= await AllPostsUseCase.execute() ;
        
        expect (posts.length).toBe(100);

        expect(posts[0].title).toBe(POSTS[0].title);
        expect(posts[0].content).toBe(POSTS[0].body);
    });
});