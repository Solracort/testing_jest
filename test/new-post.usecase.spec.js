import { NewPostUseCase } from "../src/usecases/new-post.usecase";

describe ('Send a new post use case', ()=>{

    

    it ('should send a new post to the server', async ()=>{
        const postData = { title: 'Test Post', body: 'This is a test post.' };
        const response = await NewPostUseCase.execute(postData) ;
        
        expect(response.title).toBe('Test Post');
    }, 20000);
});